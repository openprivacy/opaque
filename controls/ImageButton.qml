import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
Item {
    id: root
    property alias source: img.source
    property int size: 24
    property string tooltip: ""
    property alias color: bgRect.color
    width: size
    height: size
    signal clicked()
    property alias imgSrc: img

    ToolTip.visible: tooltip != "" && ma.containsMouse
    ToolTip.text: tooltip

    Rectangle {
    	id: bgRect
    	anchors.fill: parent
    	color: "transparent"
    }

    Image {
        id: img
        width: root.size * (ma.pressed ? 0.5 : 0.8)
        height: root.size * (ma.pressed ? 0.5 : 0.8)
        anchors.topMargin: ma.pressed ? 2 : 0
        anchors.leftMargin: anchors.topMargin
        anchors.centerIn: parent
    }

    MouseArea {
        id: ma
        anchors.fill: root

        onClicked: root.clicked()
        hoverEnabled: tooltip != ""
    }
}

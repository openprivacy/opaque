import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import "theme"


GridLayout {
    id: root

    // have children ... control weather to stack or row them
    // n * minWidth determin
    property int minCellWidth: Theme.sidePaneMinSize * gcd.themeScale


    onWidthChanged: resizeCheck()

    function resizeCheck() {
        if (width < children.length * minCellWidth) {
            root.rows = -1
            root.columns = 1
        } else {
            root.rows = 1
            root.columns = -1
        }
    }
}

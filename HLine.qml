import QtQuick 2.12
import "theme"

Column {
    width: parent.width
    anchors.horizontalCenter: typeof(Layout) == undefined ? parent.horizontalCenter : undefined

    Rectangle {
        height: 10
        color:"transparent"
        width: parent.width
    }

    Rectangle {
        anchors.horizontalCenter: parent.horizontalCenter
        height: 1
        width: parent.width
        color: Theme.dropShadowColor
    }

    Rectangle {
        height: 10
        color:"transparent"
        width: parent.width
    }
}

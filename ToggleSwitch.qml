import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick 2.12
import "theme"

// ToggleSwtch implements a stylized toggle switch.
Switch {
    style: SwitchStyle {
        handle: Rectangle {
            implicitWidth: 25
            implicitHeight: 25
            radius: width*0.5
            color: Theme.toggleColor
            border.color: checked  ? Theme.toggleOnColor  :Theme.toggleOffColor
            border.width:5
        }
        groove: Rectangle {
            implicitWidth: 50
            implicitHeight: 25
            radius: 25*0.5
            color: checked  ? Theme.toggleOnColor  :Theme.toggleOffColor
        }
    }
}



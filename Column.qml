import QtQuick 2.13

import "." as Opaque
import "theme"

Column {
	padding: Theme.paddingStandard
	spacing: Theme.paddingSmall
}
import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "." as Opaque
import "theme"
import "fonts"

Rectangle { // Global Toolbar
    id: toolbar

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top

    height: 2 * Theme.paddingSmall + Math.max(iconSize, paneTitleTextMetric.height)

    property int iconSize: Theme.uiIconSizeM


    Layout.minimumHeight: height
    Layout.maximumHeight: height
    color: Theme.toolbarMainColor

    property alias leftMenuVisible: btnLeftMenu.visible
    property alias backVisible: btnLeftBack.visible
    property alias rightMenuVisible: btnRightMenu.visible

    property int rightPaneWidth: 0


    signal leftMenu()
    signal back()
    signal rightMenu()

    Icon {
        id: btnLeftMenu
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/menu-24px.webp"


        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        width: toolbar.iconSize
        height: toolbar.iconSize

        onClicked: { leftMenu() }
    }

    Icon {
        id: btnLeftBack
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/chevron_left-24px.webp"

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        width: toolbar.iconSize
        height: toolbar.iconSize

        onClicked: { back() }
    }

   Opaque.Label {
		id: paneTitle
		width: rightPaneWidth - (btnRightMenu.visible ? btnRightMenu.width : 0) - 2 * Theme.paddingStandard
		horizontalAlignment: Text.AlignHCenter
		anchors.right: btnRightMenu.visible ? btnRightMenu.left : parent.right
		anchors.rightMargin: Theme.paddingStandard
		anchors.verticalCenter: parent.verticalCenter
		header: true
		multiline: false
		text: "global toolbar"
	}

	TextMetrics {
		id: paneTitleTextMetric
		text: paneTitle.text
		font: paneTitle.font
	}

    Icon {
        id: btnRightMenu
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/more_vert-24px.webp"

        visible: false
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        width: toolbar.iconSize
        height: toolbar.iconSize

        onClicked: { rightMenu() }
    }

    function setTitle(text) {
        paneTitle.text = text
        paneTitle.visible = true
    }

    function hideTitle() {
        paneTitle.visible = false
    }


    Connections {
        target: gcd

        onSetToolbarTitle: function(handle) {
            setTitle(handle)
            btnRightMenu.visible = true
        }
    }

}

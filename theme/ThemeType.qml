import QtQuick 2.0

QtObject {
    property color backgroundMainColor: "red"
    property color backgroundPaneColor: "red"
    property color backgroundHilightElementColor: "red"

    property color dividerColor: "red"

    property color mainTextColor: "red"
    property color altTextColor: "red"
    property color hilightElementTextColor: "red"

    property color defaultButtonColor: "red"
    property color defaultButtonActiveColor: "red"
    property color defaultButtonTextColor: "red"
    property color defaultButtonDisabledColor: "red"
    property color defaultButtonDisabledTextColor: "red"
    property color altButtonColor: "red"
    property color altButtonTextColor: "red"
    property color altButtonDisabledColor: "red"
    property color altButtonDisabledTextColor: "red"

    property color textfieldBackgroundColor: "red"
    property color textfieldBorderColor: "red"
    property color textfieldTextColor: "red"
    property color textfieldErrorColor: "red"
    property color textfieldButtonColor: "red"
    property color textfieldButtonTextColor: "red"

    property color scrollbarDefaultColor: "red"
    property color scrollbarActiveColor: "red"

    property color portraitOnlineBorderColor: "red"
    property color portraitOnlineBackgroundColor: "red"
    property color portraitOnlineTextColor: "red"
    property color portraitConnectingBorderColor: "red"
    property color portraitConnectingBackgroundColor: "red"
    property color portraitConnectingTextColor: "red"
    property color portraitOfflineBorderColor: "red"
    property color portraitOfflineBackgroundColor: "red"
    property color portraitOfflineTextColor: "red"
    property color portraitBlockedBorderColor: "red"
    property color portraitBlockedBackgroundColor: "red"
    property color portraitBlockedTextColor: "red"

    property color portraitOnlineBadgeColor: "red"
    property color portraitOfflineBadgeColor: "red"


    property color portraitContactBadgeColor: "red"
    property color portraitContactBadgeTextColor: "red"
    property color portraitProfileBadgeColor: "red"
    property color portraitProfileBadgeTextColor: "red"

    property color portraitOverlayOfflineColor: "red"

    property color dropShadowColor: "black"
    property color dropShadowPaneColor: "black"
    property color toggleColor: "black"
    property color toggleOnColor: "black"
    property color toggleOffColor: "black"
    property color sliderButtonColor: "red"
    property color sliderBarLeftColor: "red"
    property color sliderBarRightColor: "red"
    property color boxCheckedColor: "red"


    property color toolbarIconColor: "red"
    property color toolbarMainColor: "red"
    property color toolbarAltColor: "red"

    property color statusbarDisconnectedInternetColor: "red"
    property color statusbarDisconnectedInternetFontColor: "red"
    property color statusbarDisconnectedTorFontColor: "red"
    property color statusbarDisconnectedTorColor: "red"
    property color statusbarConnectingColor: "red"
    property color statusbarConnectingFontColor: "red"
    property color statusbarOnlineColor: "red"
    property color statusbarOnlineFontColor: "red"

    property color chatOverlayWarningTextColor: "red"
    property color messageFromMeBackgroundColor: "red"
    property color messageFromMeTextColor: "red"
    property color messageFromOtherBackgroundColor: "red"
    property color messageFromOtherTextColor: "red"

    property color messageStatusNormalColor: "red"
    property color messageStatusBlockedColor: "red"
    property color messageStatusBlockedTextColor: "red"
    property color messageStatusAlertColor: "red"
    property color messageStatusAlertTextColor: "red"


    // ... more to come


}

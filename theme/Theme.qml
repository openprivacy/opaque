pragma Singleton

import QtQuick 2.0

Item {
    readonly property color backgroundMainColor: theme.backgroundMainColor
    readonly property color backgroundPaneColor: theme.backgroundPaneColor
    readonly property color backgroundHilightElementColor: theme.backgroundHilightElementColor

    readonly property color dividerColor: theme.dividerColor

    readonly property color mainTextColor: theme.mainTextColor
    readonly property color altTextColor: theme.altTextColor
    readonly property color hilightElementTextColor: theme.hilightElementTextColor

    readonly property color defaultButtonColor: theme.defaultButtonColor
    readonly property color defaultButtonActiveColor: theme.defaultButtonActiveColor
    readonly property color defaultButtonTextColor: theme.defaultButtonTextColor
    readonly property color defaultButtonDisabledColor: theme.defaultButtonDisabledColor
    readonly property color defaultButtonDisabledTextColor: theme.defaultButtonDisabledTextColor
    readonly property color altButtonColor: theme.altButtonColor
    readonly property color altButtonTextColor: theme.altButtonTextColor
    readonly property color altButtonDisabledColor: theme.altButtonDisabledColor
    readonly property color altButtonDisabledTextColor: theme.altButtonDisabledTextColor

    readonly property color textfieldBackgroundColor: theme.textfieldBackgroundColor
    readonly property color textfieldBorderColor: theme.textfieldBorderColor
    readonly property color textfieldTextColor: theme.textfieldTextColor
    readonly property color textfieldErrorColor: theme.textfieldErrorColor
    readonly property color textfieldButtonColor: theme.textfieldButtonColor
    readonly property color textfieldButtonTextColor: theme.textfieldButtonTextColor


    readonly property color dropShadowColor: theme.dropShadowColor
    readonly property color dropShadowPaneColor: theme.dropShadowPaneColor

    readonly property color portraitOnlineBorderColor: theme.portraitOnlineBorderColor
    readonly property color portraitOnlineBackgroundColor: theme.portraitOnlineBackgroundColor
    readonly property color portraitOnlineTextColor: theme.portraitOnlineTextColor
    readonly property color portraitConnectingBorderColor: theme.portraitConnectingBorderColor
    readonly property color portraitConnectingBackgroundColor: theme.portraitConnectingBackgroundColor
    readonly property color portraitConnectingTextColor: theme.portraitConnectingTextColor
    readonly property color portraitOfflineBorderColor: theme.portraitOfflineBorderColor
    readonly property color portraitOfflineBackgroundColor: theme.portraitOfflineBackgroundColor
    readonly property color portraitOfflineTextColor: theme.portraitOfflineTextColor
    readonly property color portraitBlockedBorderColor: theme.portraitBlockedBorderColor
    readonly property color portraitBlockedBackgroundColor: theme.portraitBlockedBackgroundColor
    readonly property color portraitBlockedTextColor: theme.portraitBlockedTextColor

    readonly property color portraitOnlineBadgeColor: theme.portraitOnlineBadgeColor
    readonly property color portraitOfflineBadgeColor: theme.portraitOfflineBadgeColor

    readonly property color portraitContactBadgeColor: theme.portraitContactBadgeColor
    readonly property color portraitContactBadgeTextColor: theme.portraitContactBadgeTextColor
    readonly property color portraitProfileBadgeColor: theme.portraitProfileBadgeColor
    readonly property color portraitProfileBadgeTextColor: theme.portraitProfileBadgeTextColor

    readonly property color portraitOverlayOfflineColor: theme.portraitOverlayOfflineColor

    readonly property color toggleColor: theme.toggleColor
    readonly property color toggleOffColor: theme.toggleOffColor
    readonly property color toggleOnColor: theme.toggleOnColor
    readonly property color sliderButtonColor: theme.sliderButtonColor
    readonly property color sliderBarLeftColor: theme.sliderBarLeftColor
    readonly property color sliderBarRightColor: theme.sliderBarRightColor
    readonly property color boxCheckedColor: theme.boxCheckedColor


    readonly property color toolbarIconColor: theme.toolbarIconColor
    readonly property color toolbarMainColor: theme.toolbarMainColor
    readonly property color toolbarAltColor: theme.toolbarAltColor

    readonly property color statusbarDisconnectedInternetColor: theme.statusbarDisconnectedInternetColor
    readonly property color statusbarDisconnectedInternetFontColor: theme.statusbarDisconnectedInternetFontColor
    readonly property color statusbarDisconnectedTorFontColor: theme.statusbarDisconnectedTorFontColor
    readonly property color statusbarDisconnectedTorColor: theme.statusbarDisconnectedTorColor
    readonly property color statusbarConnectingColor: theme.statusbarConnectingColor
    readonly property color statusbarConnectingFontColor: theme.statusbarConnectingFontColor
    readonly property color statusbarOnlineColor: theme.statusbarOnlineColor
    readonly property color statusbarOnlineFontColor: theme.statusbarOnlineFontColor

    readonly property color chatOverlayWarningTextColor: theme.chatOverlayWarningTextColor
    readonly property color messageFromMeBackgroundColor: theme.messageFromMeBackgroundColor
    readonly property color messageFromMeTextColor: theme.messageFromMeTextColor
    readonly property color messageFromOtherBackgroundColor: theme.messageFromOtherBackgroundColor
    readonly property color messageFromOtherTextColor: theme.messageFromOtherTextColor

    readonly property color messageStatusNormalColor: theme.messageStatusNormalColor
    readonly property color messageStatusBlockedColor: theme.messageStatusBlockedColor
    readonly property color messageStatusBlockedTextColor: theme.messageStatusBlockedTextColor
    readonly property color messageStatusAlertColor: theme.messageStatusAlertColor
    readonly property color messageStatusAlertTextColor: theme.messageStatusAlertTextColor

    readonly property color scrollbarDefaultColor: theme.scrollbarDefaultColor
    readonly property color scrollbarActiveColor: theme.scrollbarActiveColor


	readonly property variant sidePaneMinSizeBase: [200, 400, 600]
    readonly property int sidePaneMinSize: sidePaneMinSizeBase[p[scale]]+200/*for debugging*/
    readonly property variant chatPaneMinSizeBase: [300, 400, 500]
    readonly property int chatPaneMinSize: chatPaneMinSizeBase[p[scale]]
    readonly property int doublePaneMinSize: sidePaneMinSize + chatPaneMinSize

    property ThemeType dark: CwtchDark{}
    property ThemeType light: CwtchLight{}
    property ThemeType theme: gcd.theme == "dark" ? dark : light

    // 0-4. replace gcd.themeScale with whatever your app has!
    property int scale: gcd.themeScaleNew

	// magnification system: all size-sets should generally respect these semantics:
	//
	//            scale
	//          0 1 2 3 4
	// padding  S M M M L
	// text     S S M L L
	//
	// use the syntax "propertyName: propertyNameBase[p[scale]]" for padding that
	// has S/M/L granularity (and likewise t[scale] for text)
	// use the syntax "propertyName: propertyNameBase[scale]" for things that you
	// would prefer have 0/1/2/3/4 granularity.

	readonly property variant p: [0, 1, 1, 1, 2]
	readonly property variant t: [0, 0, 1, 2, 2]

	//////////////////////
	// section: PADDING //
	//////////////////////

    readonly property variant paddingMinimalBase: [1, 4, 6]
	readonly property int     paddingMinimal: paddingMinimalBase[p[scale]]
	readonly property variant paddingSmallBase: [3, 10, 15]
	readonly property int     paddingSmall: paddingSmallBase[p[scale]]
    readonly property variant paddingStandardBase: [8, 20, 30]
    readonly property int     paddingStandard: paddingStandardBase[p[scale]]
    readonly property variant paddingLargeBase: [10, 30, 40]
    readonly property int     paddingLarge: paddingLargeBase[p[scale]]

    readonly property variant paddingClickTargetBase: gcd.os == "android" ? [10, 40, 100] : [3, 10, 15]
    readonly property int     paddingClickTarget: paddingClickTargetBase[p[scale]]

	////////////////////////
	// section: TEXT SIZE //
	////////////////////////

    readonly property variant textSmallPtBase: [8, 12, 16]
    readonly property int     textSmallPt: textSmallPtBase[t[scale]]
    readonly property variant textMediumPtBase: [10, 16, 24]
    readonly property int     textMediumPt: textMediumPtBase[t[scale]]
    readonly property variant textLargePtBase: [16, 24, 32]
    readonly property int     textLargePt: textLargePtBase[t[scale]]

	readonly property variant textSubHeaderPtBase: [12, 18, 26]
	readonly property int     textSubHeaderPt: textHeaderPtBase[t[scale]]
	readonly property variant textHeaderPtBase: [16, 24, 32]
	readonly property int     textHeaderPt: textHeaderPtBase[t[scale]]

    /////////////////////////////////
    // section: ELEMENT DIMENSIONS //
    /////////////////////////////////

    readonly property variant uiIconSizeSBase: [8, 16, 24]
    readonly property int uiIconSizeS: uiIconSizeSBase[p[scale]]
    readonly property variant uiIconSizeMBase: [24, 32, 48]
    readonly property int uiIconSizeM: uiIconSizeMBase[p[scale]]
    readonly property variant uiIconSizeLBase: [32, 48, 60]
    readonly property int uiIconSizeL: uiIconSizeLBase[p[scale]]

    readonly property variant uiEmojiSizeBase: [24, 32, 48]
    readonly property int uiEmojiSize: uiEmojiSizeBase[p[scale]]
    readonly property variant contactPortraitSizeBase: [60, 72, 84]
    readonly property int contactPortraitSize: contactPortraitSizeBase[p[scale]]

    ///////////////////////////////////////
    // section: OLD FONT SIZES REFERENCE //
    ///////////////////////////////////////

    // old size (usually given to font.pixelSize but occasionally to font.pointSize) -> new size
    readonly property int badgeTextSize: 12
    readonly property int statusTextSize: 12
    // readonly property int chatMetaTextSize: 15 -> textSmallPt
    // readonly property int secondaryTextSize: 20 -> textSmallPt
    readonly property int chatSize: textMediumPt //was:20
    // readonly property int primaryTextSize: 25 -> textMediumPt
    readonly property int tabSize: textMediumPt //was:25
    // readonly property int subHeaderSize: 35 -> textSubHeaderPt
    // readonly property int headerSize: 50 -> textHeaderPt
}

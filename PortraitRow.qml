import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import "styles"
import "." as Opaque
import "theme"
import "../opaque/fonts"
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: crItem
    implicitHeight: Math.max(cnMetric.height + onionMetric.height, Theme.contactPortraitSize) + Theme.paddingSmall * 2
    height: implicitHeight

    property string displayName
    property alias image: portrait.source
    property string handle
    property bool isActive
    property bool isHover
    property string tag // profile version/type

    property color rowColor: Theme.backgroundMainColor
    property color rowHilightColor: Theme.backgroundHilightElementColor
    property alias portraitBorderColor: portrait.portraitBorderColor
    property alias portraitColor: portrait.portraitColor
    property alias nameColor: cn.color
    property alias onionColor: onion.color
    property alias onionVisible: onion.visible

    property alias badgeColor: portrait.badgeColor
    property alias badgeVisible: portrait.badgeVisible
    property alias badgeContent: portrait.badgeContent

    property alias hoverEnabled: buttonMA.hoverEnabled

    // Ideally read only for icons to match against
    property alias color: crRect.color

    property alias content: extraMeta.children

    property alias portraitOverlayColor: portrait.overlayColor
    property alias portraitPerformTransform: portrait.performTransform

    signal clicked(string handle)


	// Manual columnlayout using anchors!
	// Elements on the left are bound left, elements on the right, right
	// Center element (contact name/onion) gets whatever space is left
	// because it can elide text when it becomes too small :)
	// crRect.left <- portrait <- portraitMeta -> extraMeta -> crRect.right
    Rectangle {
        id: crRect
        anchors.left: parent.left
        anchors.right: parent.right
        height: crItem.height
        width: parent.width
        // CONTACT ENTRY BACKGROUND COLOR
        color: isHover ? crItem.rowHilightColor : (isActive ? crItem.rowHilightColor : crItem.rowColor)

        Portrait {
            id: portrait
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: Theme.paddingStandard
        }

        Column {
            id: portraitMeta

            anchors.left: portrait.right
            anchors.right: extraMeta.left
            anchors.leftMargin: Theme.paddingStandard
            anchors.verticalCenter: parent.verticalCenter

            Opaque.Label { // CONTACT NAME
                id: cn
                width: parent.width
                elide: Text.ElideRight
                header: true
                text: displayName
                wrapMode: Text.NoWrap
            }

            TextMetrics {
            	id: cnMetric
            	font: cn.font
            	text: cn.text
            }

            Opaque.Label { // Onion
                id: onion
                text: handle
                width: parent.width
                elide: Text.ElideRight
                wrapMode: Text.NoWrap
            }

            TextMetrics {
            	id: onionMetric
            	font: onion.font
            	text: onion.text
            }
        }

        Column {
            id: extraMeta
            width: Theme.uiIconSizeS + 2 * Theme.paddingMinimal
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    MouseArea {  // Full row mouse area triggering onClick
        id: buttonMA
        anchors.fill: parent
        hoverEnabled: true

        onClicked: { crItem.clicked(crItem.handle) }

        onEntered: {
            isHover = true
        }

        onExited: {
            isHover = false
        }

        onCanceled: {
            isHover = false
        }

    }

    Connections { // UPDATE UNREAD MESSAGES COUNTER
        target: gcd

        onResetMessagePane: function() {
            isActive = false
        }

        onUpdateContactDisplayName: function(_handle, _displayName) {
            if (handle == _handle) {
                displayName = _displayName
            }
        }

        onUpdateContactPicture: function(_handle, _image) {
            if (handle == _handle) {
                image = _image
            }
        }
    }
}

import QtQuick 2.7
import QtQuick.Controls 2.13
import "theme"

/*!
\qmltype TextField
\inqmlmodule Opaque
\inherits QtQuick::Item
\index Index
\brief Basic single-line text input box.

For example:

*/
TextField {
    property color errorColor: Theme.textfieldErrorColor
    property bool error: false

    color: error ? errorColor : Theme.textfieldTextColor
    font.pointSize: 10 * gcd.themeScale
    width: 100

    // selectByMouse shouldn't be enabled on mobile
    selectByMouse: gcd.os != "android" && !readOnly

    background: Rectangle {
        radius: 2
        color: Theme.textfieldBackgroundColor
        border.color: error ? errorColor : Theme.textfieldBorderColor
    }
}

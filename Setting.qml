import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "." as Opaque
import "../opaque/styles"
import "../opaque/theme"


Column {
    id: tehcol

    width: parent.width - 2 * parent.padding
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: Theme.paddingSmall
    property bool inline: true
    property bool last: false

    property alias label: settingLabel.text
    property alias description: settingDescriptionLabel.text
    property alias field: fieldContainer.children



    Grid {
        id: container
        columns: inline ? 2 : 1
        spacing: Theme.paddingStandard
        padding: Theme.paddingStandard

        width: parent.width

        property int gridWidth: (inline ? (width - spacing)/2 : width) - 2*padding

        anchors.horizontalCenter: parent.horizontalCenter

        Column {
            Opaque.Label {
                id: settingLabel
                width: container.gridWidth
                header: true
                visible: text != ""
            }


            Opaque.Label {
                id: settingDescriptionLabel
                width: container.gridWidth
                size: Theme.textSmallPt
                visible: settingDescriptionLabel.text != ""
                topPadding:10
            }
        }


        Item {
            id: fieldContainer
            width: container.gridWidth
            // needs a height so !inline HLine "knows" how far down to be
            height: fieldContainer.children[0].height
        }

    }

    Opaque.HLine {
    	width: parent.width - 20
    	visible: !last
    }
}

import QtQuick 2.13
import QtQuick.Controls 2.13

import "." as Opaque
import "./theme"

// Tabs.qml
//
// Example 1:
// Tabs {
//   model: [qsTr("tab1-name"), qsTr("tab2-name"), qsTr("tab3-name")]
//   onCurrentIndexChanged: {
//     //...eg: loader.source = filenames[currentIndex]
//   }
// }
ListView {
	id: root

	// properties
	property color highlightColor: Theme.dividerColor

	// contents & appearance config
	model: ["your", "model", "here"]
	delegate: Opaque.Label {
		// contents & appearance config
		text: model.modelData
		elide: Text.ElideRight
		horizontalAlignment: Text.AlignHCenter
		size: Theme.textSmallPt
		bold: true

		// functionality
		MouseArea { anchors.fill: parent; onClicked: root.currentIndex = index; }
		width: root.width / root.model.length
	}
	highlight: Rectangle { id: hl; radius: 5; color: root.highlightColor; x: 0; width: root.width / root.model.length;}

	// functionality
	height: Theme.uiIconSizeS
	orientation: Qt.Horizontal
	interactive: true
	highlightFollowsCurrentItem: true
	boundsBehavior: Flickable.StopAtBounds
}
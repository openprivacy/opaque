import QtQuick 2.7

import QtQuick.Controls 2.13
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.12
import "." as Widgets
import "theme"

// UnderlineTextField is a textfield styled as just an underline
TextField {
    property alias backgroundColor: bg.color
    property color errorColor: Theme.textfieldErrorColor
    property bool error: false

    color:  error ? errorColor : Theme.mainTextColor
    font.pixelSize: Theme.secondaryTextSize * gcd.themeScale
    signal clicked
    smooth: true
    placeholderTextColor: Theme.altTextColor

    // NOTE: Android Password Fields don't work unless we set an explicit character.
    passwordCharacter: "*"

    background: Rectangle {
        id: bg
        anchors.fill: parent
        color: Theme.backgroundMainColor
        border.color: color
    }

    Rectangle {
        id: bottomBar
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 2
        color: error ? errorColor : Theme.mainTextColor
    }
}

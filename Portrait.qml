import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import "theme"

Item {
    id: imgProfile

    property string source
    property alias badgeColor: badge.color

    height: Theme.contactPortraitSize
    width: Theme.contactPortraitSize
    implicitWidth: Theme.contactPortraitSize
    implicitHeight: Theme.contactPortraitSize

    property alias portraitBorderColor: mainImage.color
    property alias portraitColor: imageInner.color
    property alias badgeVisible: badge.visible
    property alias badgeContent: badge.content

    property bool performTransform: false
    property alias overlayColor: iconColorOverlay.color
    property real rotationAngle: 0.0


    Rectangle {
        id: mainImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width * 0.8
        height: width
        color:  Theme.portraitOfflineBorderColor
        radius: width / 2

        Rectangle {
            id: imageInner
            width: parent.width - 4
            height: width
            color:  Theme.portraitOfflineBorderColor
            radius: width / 2
            anchors.centerIn:parent

            Image { // PROFILE IMAGE
                id: img
                source: imgProfile.source == "" ? "" : gcd.assetPath + imgProfile.source
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                visible: false

                sourceSize.width: imageInner.width*2
                sourceSize.height: imageInner.height*2
            }

            ColorOverlay{
                id: iconColorOverlay
                anchors.fill: img

                source: img
                visible: false

                antialiasing: true
                smooth: true
                transform: Rotation { origin.x: width/2; origin.y: height / 2; angle: rotationAngle}
            }

            Image { // CIRCLE MASK
                id: mask
                fillMode: Image.PreserveAspectFit
                visible: false
                source: "qrc:/qml/opaque/images/clipcircle.png"
            }

            OpacityMask {
                anchors.fill: img
                source: performTransform ? iconColorOverlay : img
                maskSource: mask
            }
        }
    }

    Badge {
        id: badge
        size: parent.width * 0.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: parent.width * 0.09
    }
}

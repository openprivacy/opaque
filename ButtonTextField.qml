import QtQuick 2.7

import QtQuick.Controls 2.13
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.12
import "." as Widgets
import "theme"


// ButtonTextField integrates a text field and a button
TextField {
    id: tf
    color:  Theme.textfieldTextColor
    font.pixelSize: Theme.secondaryTextSize * gcd.themeScale
    width: parent.width - 20

    // IMPORTANT: Setting a dynamic height on the internal button widget caused it to crash on Android when
    // resized (smaller), so we set an explicit height on TextField which seems to resolve the issue.
    height: font.pixelSize + 20
    property string icon
    property string button_text
    signal clicked
    smooth: true
    property color dropShadowColor: Theme.dropShadowColor

    background: Rectangle {
        radius: 10
        color: Theme.textfieldBackgroundColor
        border.color: Theme.textfieldBorderColor
        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            horizontalOffset: 4
            verticalOffset: 4
            samples:10
            color: tf.dropShadowColor
        }
    }

    Widgets.Button {
        icon: ""
        text: button_text
        anchors { top: parent.top; right: parent.right }
        override_radius: 10
        height: parent.height;

        inactiveColor: Theme.textfieldButtonColor
        activeColor: Theme.textfieldButtonColor
        textColor: Theme.textfieldButtonTextColor

        onClicked: {
            parent.focus = true;
            parent.clicked();
        }
    }

}

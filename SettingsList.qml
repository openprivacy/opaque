import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"



Flickable {
    id: flick
    boundsBehavior: Flickable.StopAtBounds
    contentWidth: parent.width
    clip:true
    flickableDirection: Flickable.VerticalFlick
}




import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12

import QtQuick.Templates 2.12 as T

import "../opaque" as Opaque
import "../opaque/theme"
import "../const"


T.ScrollBar {
    palette.dark: Theme.scrollbarDefaultColor
    palette.mid: Theme.scrollbarActiveColor

    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    implicitContentHeight + topPadding + bottomPadding)

    width: 10
    padding: 2
    visible: control.policy !== T.ScrollBar.AlwaysOff && control.size < 1.0

    contentItem: Rectangle {
        implicitWidth: control.interactive ? 6 : 2
        implicitHeight: control.interactive ? 6 : 2

        radius: width / 2
        color: control.pressed ?  control.palette.mid : control.palette.dark
        opacity: 1.0
    }
}

import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import "theme"
import "fonts"

// Defaults to normal size text. doesn't do its own padding!
// Setting header:true switches to header sized and bolded text
Label {
    font.pointSize: size
    wrapMode: multiline ? Text.WordWrap : Text.NoWrap
    elide: Text.ElideRight
    color: Theme.mainTextColor
    textFormat: Text.PlainText
    property bool header: false
    property real size: header ? Theme.textHeaderPt : Theme.textMediumPt
    property bool bold: header
    property bool multiline: true

    font.family: bold ? Fonts.applicationFontRegular.name : Fonts.applicationFontBold.name
    font.styleName: bold ? "Bold" : ""
}

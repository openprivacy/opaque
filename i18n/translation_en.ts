<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation>Click to cycle category.
        Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation>Click to cycle category.
        Right-click to reset.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation>Click to cycle morphs.
        Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation>Click to cycle morphs.
        Right-click to reset.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation>Click to cycle colours.
        Long-press to reset.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation>Click to cycle colours.
        Right-click to reset.</translation>
    </message>
    <message>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation>Search...</translation>
    </message>
    <message>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation>Expressions</translation>
    </message>
    <message>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation>Activities</translation>
    </message>
    <message>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation>Food, drink &amp; herbs</translation>
    </message>
    <message>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation>Gender, relationships &amp; sexuality</translation>
    </message>
    <message>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation>Nature and effects</translation>
    </message>
    <message>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation>Objects</translation>
    </message>
    <message>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation>People and animals</translation>
    </message>
    <message>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation>Symbols</translation>
    </message>
    <message>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation>Travel &amp; places</translation>
    </message>
    <message>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation>Miscellaneous</translation>
    </message>
</context>
</TS>

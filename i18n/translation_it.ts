<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation>Fare clic per scorrere le categorie.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation>Fare clic per scorrere le categorie.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation>Fare clic per scorrere i morph.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation>Fare clic per scorrere i morph.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation>Fare clic per scorrere i colori.
Pressione lunga per resettare.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation>Fare clic per scorrere i colori.
Cliccare con il tasto destro per resettare.</translation>
    </message>
    <message>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation>Espressioni</translation>
    </message>
    <message>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation>Attività</translation>
    </message>
    <message>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation>Cibo, bevande ed erbe aromatiche</translation>
    </message>
    <message>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation>Genere, relazioni e sessualità</translation>
    </message>
    <message>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation>Natura ed effetti</translation>
    </message>
    <message>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation>Oggetti</translation>
    </message>
    <message>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation>Persone e animali</translation>
    </message>
    <message>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation>Simboli</translation>
    </message>
    <message>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation>Viaggi e luoghi</translation>
    </message>
    <message>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation>Miscellanea</translation>
    </message>
</context>
</TS>

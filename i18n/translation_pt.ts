<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>EmojiDrawer</name>
    <message>
        <location filename="../EmojiDrawer.qml" line="69"/>
        <location filename="../EmojiDrawer.qml" line="95"/>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="112"/>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="121"/>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="129"/>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="137"/>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="145"/>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="153"/>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="161"/>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="169"/>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="177"/>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="185"/>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-cats-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-cats-desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="260"/>
        <source>cycle-morphs-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="260"/>
        <source>cycle-morphs-desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="293"/>
        <source>cycle-colours-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="293"/>
        <source>cycle-colours-desktop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

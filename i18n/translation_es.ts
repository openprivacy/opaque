<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>EmojiDrawer</name>
    <message>
        <source>cycle-cats-android</source>
        <translation>Click para cambiar categoría. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-cats-desktop</source>
        <translation>Click para cambiar categoría. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-morphs-android</source>
        <translation>Click para cambiar transformaciones. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-morphs-desktop</source>
        <translation>Click para cambiar transformaciones. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-colours-android</source>
        <translation>Click para cambiar de colores. Mantenga pulsado para reiniciar.</translation>
    </message>
    <message>
        <source>cycle-colours-desktop</source>
        <translation>Click para cambiar colores. Click derecho para reiniciar.</translation>
    </message>
    <message>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation>Búsqueda...</translation>
    </message>
    <message>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation>Expresiones</translation>
    </message>
    <message>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation>Actividades</translation>
    </message>
    <message>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation>Alimentos, bebidas y hierbas</translation>
    </message>
    <message>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation>Género, relaciones y sexualidad</translation>
    </message>
    <message>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation>Naturaleza y efectos</translation>
    </message>
    <message>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation>Objetos</translation>
    </message>
    <message>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation>Personas y animales</translation>
    </message>
    <message>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation>Símbolos</translation>
    </message>
    <message>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation>Viajes y lugares</translation>
    </message>
    <message>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation>Miscelánea</translation>
    </message>
</context>
</TS>

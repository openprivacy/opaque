import QtQuick 2.13

import "." as Opaque
import "theme"

Opaque.Column {
	id: root

	//: Show
	readonly property string strShow: qsTr("collapser-show")
	//: Hide
	readonly property string strHide: qsTr("collapser-hide")

	property string textShow: strShow
	property string textHide: strHide
	property bool expanded: false

	height: expanded ? childrenRect.height + 2 * root.padding : lblTitle.height + root.padding + root.spacing
	Behavior on height { PropertyAnimation {} }
	clip: true

	Opaque.Label {
		id: lblTitle
		text: root.expanded ? root.textHide + " ▲" : root.textShow + " ▼"
		header: true
		MouseArea {
			anchors.fill: parent
			onClicked: root.expanded = !root.expanded
		}
	}
}
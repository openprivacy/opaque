import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "." as Widgets
import "theme"

Rectangle {
    id: root

    property color backgroundColor: parent.color
    property color hilightBackgroundColor: backgroundColor

    property bool isHover: false

    color: isHover ? hilightBackgroundColor : backgroundColor

    property alias iconColor: iconColorOverlay.color

    property alias source: srcImg.source
    property alias sourceWidth: srcImg.sourceSize.width
    property alias sourceHeight: srcImg.sourceSize.height

    property real rotationAngle: 0.0

    signal clicked()

    signal hover(bool hover)

    property int size: Math.min(height, width)

    Image {
        id: srcImg
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        height: size
        width: size
        antialiasing: true
        smooth: true
        visible: false
        // Apparently qml can now only DOWN-SCALE/SHRINK the SVG, so with this hack it which won't cause blurriness/pixelation
        sourceSize.width: size*2
        sourceSize.height: size*2
    }

    ColorOverlay{
        id: iconColorOverlay
        anchors.fill: srcImg

        source: srcImg

        antialiasing: true
        smooth: true
        transform: Rotation { origin.x: width/2; origin.y: height / 2; angle: rotationAngle}
    }

    MouseArea {  // Full row mouse area triggering onClick
        id: ma
        anchors.fill: parent
        hoverEnabled: true

        onClicked: { root.clicked() }

        onEntered: {
            isHover = true
            root.hover(true)
        }

        onExited: {
            isHover = false
            root.hover(false)
        }
    }

}
